package ictu.edu.vn.android.flexibleui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showFragment(this, R.id.layoutFragmentDealList, new DealListFragment(), false);
    }

    public static void showFragment(AppCompatActivity activity, int layoutId,
                                    Fragment fragment, boolean isBack) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        ft.replace(layoutId, fragment);
        if (isBack) ft.addToBackStack(null);
        ft.commit();
    }

    public static String getTextDate(Date date) {
        return new SimpleDateFormat("dd/mm/yyyy").format(date);
    }
}


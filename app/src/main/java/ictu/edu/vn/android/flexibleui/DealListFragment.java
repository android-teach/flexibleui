package ictu.edu.vn.android.flexibleui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DealListFragment extends Fragment implements DealAdapter.OnItemClick {
    private List<Deal> dealList = new ArrayList<>();
    private DealAdapter adapter;
    private boolean isLand = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deal_list, container, false);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        adapter = new DealAdapter(getContext(), dealList);
        adapter.setOnItemClickListener(this);
        RecyclerView rvDeal = view.findViewById(R.id.rvDeal);
        rvDeal.setLayoutManager(layoutManager);
        rvDeal.setAdapter(adapter);

        loadData();
        isLand = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        if (isLand) showDetailFragment(0);

        return view;
    }

    private void loadData() {
        Deal deal1 = new Deal(1, "Đi ăn quán", "Chán nấu nên ăn quán", 30000, new Date(), R.drawable.ic_restaurant);
        Deal deal2 = new Deal(2, "Ăn mì tôm", "Quán nấu chán, lại mua mì tôm", 20000, new Date(), R.drawable.ic_restaurant);
        Deal deal3 = new Deal(3, "Xăng xe", "Đi ăn xa quá nên hết xăng", 50000, new Date(), R.drawable.ic_motorcycle);

        dealList.add(deal1);
        dealList.add(deal2);
        dealList.add(deal3);

        adapter.notifyDataSetChanged();
    }

    private void showDetailFragment(int position) {
        if (position >= dealList.size()) return;

        AppCompatActivity activity = (AppCompatActivity) getActivity();

        Deal deal = dealList.get(position);
        if (isLand) {
            MainActivity.showFragment(activity, R.id.layoutFragmentDetailDeal, DealDetailFragment.newInstance(deal), false);
        } else {
            MainActivity.showFragment(activity, R.id.layoutFragmentDealList, DealDetailFragment.newInstance(deal), true);
        }
    }

    @Override
    public void onItemClick(int position) {
        showDetailFragment(position);
    }
}


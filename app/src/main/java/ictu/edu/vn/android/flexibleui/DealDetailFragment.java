package ictu.edu.vn.android.flexibleui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

public class DealDetailFragment extends Fragment {
    public static final String KEY_DEAL = "deal";

    public static DealDetailFragment newInstance(Deal deal) {
        DealDetailFragment fragment = new DealDetailFragment();
        Bundle args = new Bundle();
        args.putString(KEY_DEAL, new Gson().toJson(deal));
        fragment.setArguments(args);
        return fragment;
    }

    private Deal deal;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String json = getArguments().getString(KEY_DEAL);
        deal = new Gson().fromJson(json, Deal.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.deal_detail, container, false);

        TextView tvName = view.findViewById(R.id.tvDealName);
        TextView tvPrice = view.findViewById(R.id.tvPrice);
        TextView tvNote = view.findViewById(R.id.tvNote);
        TextView tvDate = view.findViewById(R.id.tvDate);
        ImageView ivThumbnail = view.findViewById(R.id.ivThumbnail);

        if (deal != null) {
            tvName.setText(deal.getName());
            tvPrice.setText(deal.getPrice() + " đ");
            tvNote.setText(deal.getNote());
            tvDate.setText(MainActivity.getTextDate(deal.getDate()));
            ivThumbnail.setImageResource(deal.getGroupId());
        }

        return view;
    }
}

